package password.logger;

import authorisation.logger.UserPrefix;
import config.ConfigEnvironment;
import logger.main.Logger;
import logger.main.prefixs.TimePrefix;

public class PasswordServerLogger extends Logger{

	private static final boolean debug = Boolean.parseBoolean(ConfigEnvironment.getProperty("Status.PWServer.Debug"));

	private static final PasswordServerLogger logger = new PasswordServerLogger();
	
	private PasswordServerLogger() {
		super("PWServer", new TimePrefix("dd.MM.yy"), new TimePrefix("hh:mm:ss"), new UserPrefix());
	}
	
	public static void debug(String msg) {
		if(!debug) return;
		logger.println("[debug] " + msg);
	}
	
	public static void debug(String msg, Throwable e) {
		if(!debug) return;
		logger.println("[debug] " + msg + "->  An error occured: " + e.getMessage());
	}

	public static void info(String msg) {
		logger.println("[info] " + msg);
	}

	public static void info(String msg, long clientId) {
		logger.println("[info] " + "[" + clientId + "] " + msg);
	}

	public static void info(String msg, String userName) {
		logger.println("[info] " + "[" + userName + "] " + msg);
	}

}
