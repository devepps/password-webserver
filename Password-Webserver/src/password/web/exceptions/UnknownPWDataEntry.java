package password.web.exceptions;

import server.data.exceptions.HTTPException;
import server.data.type.ResponseType;

public class UnknownPWDataEntry extends HTTPException {

	private static final long serialVersionUID = 1L;

	public UnknownPWDataEntry() {
		super(ResponseType.BAD_REQUEST, "Kein Datensatz gefunden.");
	}

}
