package password.web.controller;

import java.util.ArrayList;
import java.util.Arrays;

import authorisation.web.interceptor.UserSecurity;
import password.logger.PasswordServerLogger;
import password.web.dto.PWDataSetDTO;
import password.web.service.PWDataSetService;
import password.web.service.PWDataServiceManager;
import server.data.controller.Controller;
import server.data.interceptor.before.Before;
import server.data.parameter.ParameterConfig;
import server.data.requestAcceptor.RequestAcceptor;
import server.data.responseMessage.ResponseMessage;
import server.data.type.ContentType;
import server.data.type.ParameterType;
import server.data.type.RequestType;
import server.data.type.ResponseType;

@Controller(basePath = "pwData")
public class PWDataSetController {

	private PWDataSetService pwDataService = PWDataServiceManager.SERVICE_MANAGER.getPwDataSetService();

	@RequestAcceptor(path = "Sets/Ids", requestType = RequestType.GET)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage getPasswordDataSetIds() {

		ArrayList<Integer> dataSets = pwDataService.getPasswordDataSetIds();
		Object[] dataSetArray = new Object[dataSets.size()];
		dataSetArray = dataSets.toArray(dataSetArray);
		PasswordServerLogger.debug("Answer Password-Sets found: " + Arrays.toString(dataSetArray));

		return new ResponseMessage(ResponseType.OK, dataSetArray);
	}

	@RequestAcceptor(path = "Sets/Data", requestType = RequestType.GET)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage getPasswordDataSets() {

		ArrayList<PWDataSetDTO> dataSets = pwDataService.getPasswordDataSets();
		Object[] dataSetArray = new Object[dataSets.size()];
		dataSetArray = dataSets.toArray(dataSetArray);
		PasswordServerLogger.debug("Answer Password-Sets found: " + Arrays.toString(dataSetArray));

		return new ResponseMessage(ResponseType.OK, dataSetArray);
	}

	@RequestAcceptor(path = "Set", requestType = RequestType.GET)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage getPasswordDataSet(
			@ParameterConfig(contentType = ContentType.BASE_TYPE, parameterType = ParameterType.URL, name = "id") Integer pwDataSetId) {

		PWDataSetDTO dataSet = pwDataService.getPasswordDataSet(pwDataSetId);

		return new ResponseMessage(ResponseType.OK, dataSet);
	}

	@RequestAcceptor(path = "Set", requestType = RequestType.PUT)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage addPasswordData(
			@ParameterConfig(name = "pwDataSet", contentType = ContentType.JSON_OBJECT) PWDataSetDTO pwDataSetDTO) {

		PWDataSetDTO dataSet = pwDataService.addPasswordDataSet(pwDataSetDTO);

		return new ResponseMessage(ResponseType.OK, dataSet);
	}

	@RequestAcceptor(path = "Set", requestType = RequestType.POST)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage updatePasswordData(
			@ParameterConfig(name = "pwDataSet", contentType = ContentType.JSON_OBJECT) PWDataSetDTO pwDataSetDTO,
			@ParameterConfig(name = "id", parameterType = ParameterType.URL) Integer id) {

		PasswordServerLogger.debug("Password-Set-Update found: " + pwDataSetDTO);
		PWDataSetDTO dataSet = pwDataService.updatePasswordDataSet(id, pwDataSetDTO);

		return new ResponseMessage(ResponseType.OK, dataSet);
	}

	@RequestAcceptor(path = "Set", requestType = RequestType.DELETE)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage deletePasswordData(
			@ParameterConfig(name = "id", parameterType = ParameterType.URL) Integer id) {

		pwDataService.deletePasswordDataSet(id);

		return new ResponseMessage(ResponseType.OK);
	}
}
