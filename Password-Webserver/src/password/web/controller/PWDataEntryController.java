package password.web.controller;

import authorisation.web.interceptor.UserSecurity;
import password.web.dto.PWDataEntryDTO;
import password.web.service.PWDataEntryService;
import password.web.service.PWDataServiceManager;
import server.data.controller.Controller;
import server.data.interceptor.before.Before;
import server.data.parameter.ParameterConfig;
import server.data.requestAcceptor.RequestAcceptor;
import server.data.responseMessage.ResponseMessage;
import server.data.type.ContentType;
import server.data.type.ParameterType;
import server.data.type.RequestType;
import server.data.type.ResponseType;

@Controller(basePath = "pwData")
public class PWDataEntryController {

	private PWDataEntryService pwDataService = PWDataServiceManager.SERVICE_MANAGER.getPwDataEntryService();

	@RequestAcceptor(path = "Entry", requestType = RequestType.PUT)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage addPasswordData(
			@ParameterConfig(name = "pwDataEntry", contentType = ContentType.JSON_OBJECT) PWDataEntryDTO pwDataEntryDTO,
			@ParameterConfig(name = "setId", parameterType = ParameterType.URL) Integer setId) {

		PWDataEntryDTO dataEntry = pwDataService.addPasswordDataEntry(setId, pwDataEntryDTO);

		return new ResponseMessage(ResponseType.OK, dataEntry);
	}

	@RequestAcceptor(path = "Entry", requestType = RequestType.POST)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage updatePasswordData(
			@ParameterConfig(name = "pwDataEntry", contentType = ContentType.JSON_OBJECT) PWDataEntryDTO pwDataEntryDTO,
			@ParameterConfig(name = "setId", parameterType = ParameterType.URL) Integer setId) {

		PWDataEntryDTO dataEntry = pwDataService.updatePasswordDataEntry(setId, pwDataEntryDTO);

		return new ResponseMessage(ResponseType.OK, dataEntry);
	}

	@RequestAcceptor(path = "Entry", requestType = RequestType.DELETE)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage deletePasswordData(
			@ParameterConfig(name = "entryId", parameterType = ParameterType.URL) Integer entryId,
			@ParameterConfig(name = "setId", parameterType = ParameterType.URL) Integer setId) {

		pwDataService.deletePasswordDataEntry(entryId, setId);

		return new ResponseMessage(ResponseType.OK);
	}
}
