package password.web.service;

import java.util.ArrayList;
import java.util.Arrays;

import authorisation.web.exceptions.NoPermissionException;
import authorisation.web.exceptions.NotLoggedinException;
import authorisation.web.service.AuthorisationServiceManager;
import authorisation.web.service.SecurityService;
import password.java.PWDataServer;
import password.java.repo.PWDataEntry;
import password.java.repo.PWDataSet;
import password.logger.PasswordServerLogger;
import password.web.dto.PWDataEntryDTO;
import password.web.dto.PWDataSetDTO;
import password.web.exceptions.UnknownPWDataSet;
import userSystem.data.ValidatedUser;

public class PWDataSetService {

	private SecurityService securityService = AuthorisationServiceManager.SERVICE_MANAGER.getSecurityService();
	private PWDataServer server = PWDataServer.PW_DATA_SERVER;

	public ArrayList<Integer> getPasswordDataSetIds() {
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}

		ArrayList<Integer> dataSetIds = new ArrayList<>();
		ArrayList<PWDataSet> dataSets = server.getPWDataSets(currentUser);
		for (PWDataSet pwDataSet : dataSets) {
			dataSetIds.add(pwDataSet.getId());
		}
		return dataSetIds;
	}
	
	public ArrayList<PWDataSetDTO> getPasswordDataSets() {
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}

		ArrayList<PWDataSetDTO> dataSetDTOs = new ArrayList<>();
		ArrayList<PWDataSet> dataSets = server.getPWDataSets(currentUser);
		for (PWDataSet pwDataSet : dataSets) {
			PWDataEntryDTO[] pwDatas = new PWDataEntryDTO[pwDataSet.getDataIds().size()];
			for (int i = 0; i < pwDataSet.getDataIds().size(); i++) {
				int id = pwDataSet.getDataIds().get(i);
				PWDataEntry pwData = server.getPWData(id);
				if (pwData == null) {
					PasswordServerLogger.info("PW-DataSet not found: " + pwDataSet.getDataIds().get(i), currentUser.getUser().getName());
					server.removePWData(id);
					pwDataSet.getDataIds().remove(i);
					i -= 1;
				} else {
					pwDatas[i] = new PWDataEntryDTO(pwData);
				}
			}
			pwDatas = Arrays.copyOfRange(pwDatas, 0, pwDataSet.getDataIds().size());
			dataSetDTOs.add(new PWDataSetDTO(pwDataSet, pwDatas));
		}
		return dataSetDTOs;
	}

	public PWDataSetDTO getPasswordDataSet(Integer pwDataSetId) {
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}
		if (pwDataSetId == null) {
			throw new UnknownPWDataSet();
		}
		PWDataSet pwDataSet = server.getPWDataSet(pwDataSetId);
		if (pwDataSet == null) {
			throw new UnknownPWDataSet();
		}

		PWDataEntryDTO[] pwDatas = new PWDataEntryDTO[pwDataSet.getDataIds().size()];
		for (int i = 0; i < pwDatas.length; i++) {
			PWDataEntry pwData = server.getPWData(pwDataSet.getDataIds().get(i));
			pwDatas[i] = new PWDataEntryDTO(pwData);
		}
		return new PWDataSetDTO(pwDataSet, pwDatas);
	}

	public PWDataSetDTO addPasswordDataSet(PWDataSetDTO pwDataSetDTO) {
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}
		PWDataSet pwDataSet = new PWDataSet(0, currentUser.getUser().getId(), pwDataSetDTO.getName());
		pwDataSet = server.addPWDataSet(pwDataSet);
		for (PWDataEntryDTO pwDataDTO : pwDataSetDTO.getContent()) {
			PWDataEntry pwData = server.addPWData(new PWDataEntry(0, pwDataDTO.getKey(), pwDataDTO.getValue(), pwDataDTO.isSecured(), pwDataDTO.getCreated()));
			pwDataSet.getDataIds().add(pwData.getId());
		}
		return new PWDataSetDTO(pwDataSet.getId(), pwDataSet.getName(), pwDataSetDTO.getContent(), pwDataSet.getCreated());
	}

	public PWDataSetDTO updatePasswordDataSet(Integer id, PWDataSetDTO pwDataSetDTO) {
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}
		PWDataSet pwDataSet = server.getPWDataSet(id);
		if (pwDataSet == null) {
			throw new UnknownPWDataSet();
		}
		if (!pwDataSet.getUserId().equals(currentUser.getUser().getId())) {
			throw new NoPermissionException();
		}
		pwDataSet.setName(pwDataSetDTO.getName());

		for (int i = 0; i < pwDataSetDTO.getContent().length; i++) {
			PasswordServerLogger.debug("Set-Update-" + i + ": " + pwDataSetDTO + " -> " + pwDataSet);
			PWDataEntryDTO pwDataDTO = pwDataSetDTO.getContent()[i];
			PWDataEntry pwData = server.getPWData(pwDataDTO.getId());
			if (pwData == null) {
				pwData = server.addPWData(new PWDataEntry(0, pwDataDTO.getKey(), pwDataDTO.getValue(), pwDataDTO.isSecured(), pwDataDTO.getCreated()));
				pwDataSet.getDataIds().add(pwData.getId());
			} else {
				pwData = server.updatePWData(new PWDataEntry(pwDataDTO));
			}
			pwDataSetDTO.getContent()[i] = new PWDataEntryDTO(pwData);
		}
		PasswordServerLogger.debug("Set-Update: " + pwDataSetDTO + " -> " + pwDataSet);
		pwDataSet = server.updatePWDataSet(pwDataSet);
		PasswordServerLogger.debug("Set-Update: " + pwDataSetDTO + " -> " + pwDataSet);
		return new PWDataSetDTO(pwDataSet.getId(), pwDataSet.getName(), pwDataSetDTO.getContent(), pwDataSet.getCreated());
	}

	public void deletePasswordDataSet(Integer id) {
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}
		PWDataSet pwDataSet = server.getPWDataSet(id);
		if (pwDataSet == null) {
			throw new UnknownPWDataSet();
		}
		if (!pwDataSet.getUserId().equals(currentUser.getUser().getId())) {
			throw new NoPermissionException();
		}
		server.removePWDataSet(pwDataSet);
	}

}
