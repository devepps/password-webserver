package password.web.service;

public class PWDataServiceManager {

	public static final PWDataServiceManager SERVICE_MANAGER = new PWDataServiceManager();

	private PWDataSetService pwDataSetService;
	private PWDataEntryService pwDataEntryService;

	private PWDataServiceManager() {
	}

	public PWDataServiceManager create() {
		this.pwDataSetService = new PWDataSetService();
		this.pwDataEntryService = new PWDataEntryService();
		return this;
	}

	public PWDataSetService getPwDataSetService() {
		return pwDataSetService;
	}

	public PWDataEntryService getPwDataEntryService() {
		return pwDataEntryService;
	}

}
