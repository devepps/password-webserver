package password.web.service;

import authorisation.web.exceptions.NoPermissionException;
import authorisation.web.exceptions.NotLoggedinException;
import authorisation.web.service.AuthorisationServiceManager;
import authorisation.web.service.SecurityService;
import password.java.PWDataServer;
import password.java.repo.PWDataEntry;
import password.java.repo.PWDataSet;
import password.web.dto.PWDataEntryDTO;
import password.web.exceptions.UnknownPWDataEntry;
import password.web.exceptions.UnknownPWDataSet;
import userSystem.data.ValidatedUser;

public class PWDataEntryService {

	private SecurityService securityService = AuthorisationServiceManager.SERVICE_MANAGER.getSecurityService();
	private PWDataServer javaServer = PWDataServer.PW_DATA_SERVER;

	public PWDataEntryDTO addPasswordDataEntry(Integer setId, PWDataEntryDTO pwDataEntryDTO) {
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}
		PWDataSet pwDataSet = javaServer.getPWDataSet(setId);
		if (pwDataSet == null) {
			throw new UnknownPWDataSet();
		}
		if (!pwDataSet.getUserId().equals(currentUser.getUser().getId())) {
			throw new NoPermissionException();
		}
		PWDataEntry pwDataEntry = javaServer
				.addPWData(new PWDataEntry(0, pwDataEntryDTO.getKey(), pwDataEntryDTO.getValue()));
		pwDataSet.getDataIds().add(pwDataEntry.getId());
		javaServer.updatePWDataSet(pwDataSet);
		return new PWDataEntryDTO(pwDataEntry);
	}

	public PWDataEntryDTO updatePasswordDataEntry(Integer setId, PWDataEntryDTO pwDataEntryDTO) {
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}
		PWDataSet pwDataSet = javaServer.getPWDataSet(setId);
		if (pwDataSet == null) {
			throw new UnknownPWDataSet();
		}
		if (!pwDataSet.getUserId().equals(currentUser.getUser().getId())) {
			throw new NoPermissionException();
		}
		if (!pwDataSet.getDataIds().contains(pwDataEntryDTO.getId())) {
			throw new UnknownPWDataEntry();
		}
		PWDataEntry pwDataEntry = javaServer
				.updatePWData(new PWDataEntry(0, pwDataEntryDTO.getKey(), pwDataEntryDTO.getValue()));
		return new PWDataEntryDTO(pwDataEntry);
	}

	public void deletePasswordDataEntry(Integer entryId, Integer setId) {
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}
		PWDataSet pwDataSet = javaServer.getPWDataSet(setId);
		if (pwDataSet == null) {
			throw new UnknownPWDataSet();
		}
		if (!pwDataSet.getUserId().equals(currentUser.getUser().getId())) {
			throw new NoPermissionException();
		}
		if (!pwDataSet.getDataIds().contains(entryId)) {
			throw new UnknownPWDataEntry();
		}
		PWDataEntry pwDataEntry = javaServer.getPWData(entryId);
		if (pwDataEntry == null) {
			throw new UnknownPWDataEntry();
		}
		pwDataSet.getDataIds().remove(entryId);
		javaServer.updatePWDataSet(pwDataSet);
		javaServer.removePWData(entryId);
	}

}
