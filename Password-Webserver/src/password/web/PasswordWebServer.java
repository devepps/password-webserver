package password.web;

import authorisation.web.AuthorisationWebServer;
import password.web.controller.PWDataEntryController;
import password.web.controller.PWDataSetController;
import password.web.dto.PWDataEntryDTO;
import password.web.dto.PWDataSetDTO;
import password.web.service.PWDataServiceManager;
import server.serverManager.ServerBuilder;

public class PasswordWebServer {

	private AuthorisationWebServer aws;

	public PasswordWebServer() {
		aws = new AuthorisationWebServer();
		PWDataServiceManager.SERVICE_MANAGER.create();
	}

	public void buildOwn() {
		ServerBuilder sb = new ServerBuilder("password.web.server.port", "password.web.keyStore.path", "password.web.keyStore.password");
		this.aws.build(sb);
		this.build(sb);
		sb.build();
	}

	public ServerBuilder build(ServerBuilder serverBuilder) {
		return serverBuilder
				.addJSONType(PWDataEntryDTO.class)
				.addJSONType(PWDataSetDTO.class)
				.addController(PWDataEntryController.class)
				.addController(PWDataSetController.class);
	}

}
