package password.web.dto;

import java.time.LocalDateTime;

import json.JSON;
import json.jsoner.StringByte;
import password.java.repo.PWDataEntry;

@JSON(name = "passwordData")
public class PWDataEntryDTO {

	private Integer id;
	private StringByte key;
	private StringByte content;
	private Boolean isSecure;
	private LocalDateTime created;

	public PWDataEntryDTO() {
		super();
	}

	public PWDataEntryDTO(Integer id, byte[] key, byte[] content) {
		super();
		this.id = id;
		this.key = new StringByte(key);
		this.content = new StringByte(content);
	}

	public PWDataEntryDTO(Integer id, StringByte key, StringByte content, Boolean isSecured, LocalDateTime created) {
		super();
		this.id = id;
		this.key = key;
		this.content = content;
		this.isSecure = isSecured;
		this.created = created;
	}

	public PWDataEntryDTO(PWDataEntry pwData) {
		this.id = pwData.getId();
		this.key = new StringByte(pwData.getKey());
		this.content = new StringByte(pwData.getValue());
		this.isSecure = pwData.isSecured();
		this.created = pwData.getCreated();
	}

	public byte[] getKey() {
		return key.getByte();
	}

	public byte[] getValue() {
		return content.getByte();
	}

	public Integer getId() {
		return id;
	}

	public Boolean isSecured() {
		return isSecure;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	@Override
	public String toString() {
		return "PWDataEntryDTO [id=" + id + ", key=" + key + ", content=" + content + ", isSecured=" + isSecure
				+ ", created=" + created + "]";
	}

}
