package password.web.dto;

import java.time.LocalDateTime;
import java.util.Arrays;

import json.JSON;
import password.java.repo.PWDataSet;

@JSON(name = "passwordDataSet")
public class PWDataSetDTO {

	private Integer id;
	private String name;
	private PWDataEntryDTO[] entries;
	private LocalDateTime created;

	public PWDataSetDTO() {
		super();
	}

	public PWDataSetDTO(Integer id, String name, PWDataEntryDTO[] entries, LocalDateTime created) {
		super();
		this.id = id;
		this.name = name;
		this.entries = entries;
		this.created = created;
	}

	public PWDataSetDTO(int id, String name, PWDataEntryDTO[] content) {
		super();
		this.id = id;
		this.name = name;
		this.entries = content;
	}

	public PWDataSetDTO(PWDataSet pwDataSet, PWDataEntryDTO[] pwDatas) {
		this.id = pwDataSet.getId();
		this.name = pwDataSet.getName();
		this.entries = pwDatas;
		this.created = pwDataSet.getCreated();
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public PWDataEntryDTO[] getContent() {
		return entries;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	@Override
	public String toString() {
		return "PWDataSetDTO [id=" + id + ", name=" + name + ", entries=" + Arrays.toString(entries) + ", created="
				+ created + "]";
	}

}
