package password.java;

import java.util.ArrayList;

import collection.tick.TickManager;
import password.java.repo.PWDataEntry;
import password.java.repo.PWDataSet;
import repositoryClient.client.RepoClientInterface;
import repositoryClient.data.Repository;
import userSystem.data.ValidatedUser;

public class PWDataServer {

	public static PWDataServer PW_DATA_SERVER;

	private Repository<PWDataEntry> pwDataRepository;
	private Repository<PWDataSet> pwDataSetRepository;

	private RepoClientInterface repoClient;

	public PWDataServer() {
		this.repoClient = new RepoClientInterface("Repository.Client.Connection.User-PW.Ip", "Repository.Client.Connection.User-PW.Port", "Repository.Client.Connection.User-PW.keyStorePath");

		this.pwDataRepository = repoClient.createRepository(PWDataEntry.class);
		this.pwDataSetRepository = repoClient.createRepository(PWDataSet.class);

		new TickManager(() -> repoClient.tick(), 1);
		
		PW_DATA_SERVER = this;
	}

	public ArrayList<PWDataSet> getPWDataSets(ValidatedUser user) {
		ArrayList<PWDataSet> dataSets = pwDataSetRepository.getByData(user.getUser().getId(), "userId");
		return dataSets;
	}

	public void removePWData(Integer id) {
		pwDataRepository.removeByKey(id);
	}

	public PWDataEntry getPWData(long id) {
		return pwDataRepository.getByKey(id);
	}

	public PWDataEntry addPWData(PWDataEntry pwData) {
		return pwDataRepository.add(pwData);
	}

	public PWDataEntry updatePWData(PWDataEntry pwDataEntry) {
		return pwDataRepository.set(pwDataEntry);
	}

	public PWDataSet addPWDataSet(PWDataSet pwDataSet) {
		return pwDataSetRepository.add(pwDataSet);
	}

	public PWDataSet getPWDataSet(Integer id) {
		return pwDataSetRepository.getByKey(id);
	}

	public PWDataSet updatePWDataSet(PWDataSet pwDataSet) {
		return pwDataSetRepository.set(pwDataSet);
	}

	public void removePWDataSet(PWDataSet pwDataSet) {
		pwDataSetRepository.removeByKey(pwDataSet.getId());
		for (int entryId : pwDataSet.getDataIds()) {
			removePWData(entryId);
		}
	}

}
