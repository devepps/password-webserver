package password;

import password.java.PWDataServer;
import password.web.PasswordWebServer;

public class PasswordServer {

	public static void main(String[] args) {
		new PasswordServer();
	}

	public PasswordServer() {
		new PWDataServer();
		new PasswordWebServer().buildOwn();
	}

}
