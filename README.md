Configuration-Properties:

<pre>
Status:
    SCBase:
        Debug: Debug-Status (true/false)
Connection:
    Max_Heartbeat_Duration: Max wait time until next heartbeat before connection is closed (in ms)
    Data: Data-Reader-Id (int) must match between communicating services
        Reader-ID:
            Single:
				Header:
				Integer:
				Boolean:
				Byte_Array:
				Byte:
				String:
					ISO_8859_1:
					Unicode:
				Double:
				Null:
				Long:
				Subscribed_Messages_Header:
				Serializable:
				Date-Time:
				Time:
				Date:
				UUID:
			Special:
				SubEntity:
			Multi:
				Single:
				ArrayList:
    DTO-Ids: Data-Reader-Id (int) must match between communicating services
		internel:
			HeartBeat:
			Kick:
			SubscribedMessage:
</pre>