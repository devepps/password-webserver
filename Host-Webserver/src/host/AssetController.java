package host;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import server.data.controller.Controller;
import server.data.requestAcceptor.PureRequestAcceptor;
import server.data.responseMessage.PureResponseMessage;
import server.data.streams.TransferStream;
import server.data.type.RequestType;
import server.data.type.ResponseType;

@Controller(basePath = "host/assets")
public class AssetController {
	
	private static final String BASE_PATH = "./dist/assets/";

	@PureRequestAcceptor(path = "icon/delete_forever-icon.svg", requestType = RequestType.GET)
	public PureResponseMessage getDeleteForeverIcon() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "icon/delete_forever-icon.svg"))), "image/svg; charset=iso-8859-1");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "image/svg; charset=iso-8859-1");
		}
	}

	@PureRequestAcceptor(path = "icon/edit-icon.svg", requestType = RequestType.GET)
	public PureResponseMessage getEditIcon() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "icon/edit-icon.svg"))), "image/svg; charset=iso-8859-1");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "image/svg; charset=iso-8859-1");
		}
	}

	@PureRequestAcceptor(path = "icon/visible-icon.svg", requestType = RequestType.GET)
	public PureResponseMessage getVisibleIcon() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "icon/visible-icon.svg"))), "image/svg; charset=iso-8859-1");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "image/svg; charset=iso-8859-1");
		}
	}

	@PureRequestAcceptor(path = "icon/not-visible-icon.svg", requestType = RequestType.GET)
	public PureResponseMessage getNotVisibleIcon() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "icon/not-visible-icon.svg"))), "image/svg; charset=iso-8859-1");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "image/svg; charset=iso-8859-1");
		}
	}

	@PureRequestAcceptor(path = "icon/arrow-down-icon.svg", requestType = RequestType.GET)
	public PureResponseMessage getArrowDownIcon() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "icon/arrow-down-icon.svg"))), "image/svg; charset=iso-8859-1");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "image/svg; charset=iso-8859-1");
		}
	}

	@PureRequestAcceptor(path = "icon/arrow-up-icon.svg", requestType = RequestType.GET)
	public PureResponseMessage getArrowUpIcon() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "icon/arrow-up-icon.svg"))), "image/svg; charset=iso-8859-1");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "image/svg; charset=iso-8859-1");
		}
	}

	@PureRequestAcceptor(path = "icon/arrow-right-icon.svg", requestType = RequestType.GET)
	public PureResponseMessage getRightUpIcon() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "icon/arrow-right-icon.svg"))), "image/svg; charset=iso-8859-1");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "image/svg; charset=iso-8859-1");
		}
	}

	@PureRequestAcceptor(path = "icon/clipboard-copy-icon.svg", requestType = RequestType.GET)
	public PureResponseMessage getClipboardCopyIcon() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "icon/clipboard-copy-icon.svg"))), "image/svg; charset=iso-8859-1");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "image/svg; charset=iso-8859-1");
		}
	}

	@PureRequestAcceptor(path = "icon/upload-icon.svg", requestType = RequestType.GET)
	public PureResponseMessage getUploadIcon() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "icon/upload-icon.svg"))), "image/svg; charset=iso-8859-1");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "image/svg; charset=iso-8859-1");
		}
	}

	@PureRequestAcceptor(path = "icon/download-icon.svg", requestType = RequestType.GET)
	public PureResponseMessage getDownloadIcon() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "icon/download-icon.svg"))), "image/svg; charset=iso-8859-1");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "image/svg; charset=iso-8859-1");
		}
	}

	@PureRequestAcceptor(path = "icon/file-icon.svg", requestType = RequestType.GET)
	public PureResponseMessage getFileIcon() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "icon/file-icon.svg"))), "image/svg; charset=iso-8859-1");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "image/svg; charset=iso-8859-1");
		}
	}

	@PureRequestAcceptor(path = "icon/folder-icon.svg", requestType = RequestType.GET)
	public PureResponseMessage getFolderIcon() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "icon/folder-icon.svg"))), "image/svg; charset=iso-8859-1");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "image/svg; charset=iso-8859-1");
		}
	}

	@PureRequestAcceptor(path = "icon/file-upload-icon.svg", requestType = RequestType.GET)
	public PureResponseMessage getFileUploadIcon() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "icon/file-upload-icon.svg"))), "image/svg; charset=iso-8859-1");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "image/svg; charset=iso-8859-1");
		}
	}

	@PureRequestAcceptor(path = "icon/folder-upload-icon.svg", requestType = RequestType.GET)
	public PureResponseMessage getFoldeUploadrIcon() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "icon/folder-upload-icon.svg"))), "image/svg; charset=iso-8859-1");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "image/svg; charset=iso-8859-1");
		}
	}

	@PureRequestAcceptor(path = "icon/create-new-folder-icon.svg", requestType = RequestType.GET)
	public PureResponseMessage getCreateNewFolderIcon() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "icon/create-new-folder-icon.svg"))), "image/svg; charset=iso-8859-1");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "image/svg; charset=iso-8859-1");
		}
	}

	@PureRequestAcceptor(path = "icon/more-horiz-icon.svg", requestType = RequestType.GET)
	public PureResponseMessage getMoreHorizontalIcon() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "icon/more-horiz-icon.svg"))), "image/svg; charset=iso-8859-1");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "image/svg; charset=iso-8859-1");
		}
	}

}
