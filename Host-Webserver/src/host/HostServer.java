package host;

import server.serverManager.ServerBuilder;

public class HostServer {

	public static void main(String[] args) {
		new ServerBuilder("host.web.server.port", "host.web.keyStore.path", "host.web.keyStore.password")
				.addController(HostController.class).addController(AssetController.class).build();
	}

}
