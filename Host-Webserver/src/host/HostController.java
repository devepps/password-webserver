package host;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import server.data.controller.Controller;
import server.data.requestAcceptor.PureRequestAcceptor;
import server.data.responseMessage.PureResponseMessage;
import server.data.streams.TransferStream;
import server.data.type.RequestType;
import server.data.type.ResponseType;

@Controller(basePath = "host")
public class HostController {
	
	private static final String BASE_PATH = "./dist/";

	@PureRequestAcceptor(path = "", requestType = RequestType.GET)
	public PureResponseMessage getIndexHTML() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "index.html"))), "text/html; charset=iso-8859-1");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "text/html; charset=iso-8859-1");
		}
	}

	@PureRequestAcceptor(path = "favicon.ico", requestType = RequestType.GET)
	public PureResponseMessage getFaviconION() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "favicon.ico"))), "image/x-icon");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "image/x-icon");
		}
	}

	@PureRequestAcceptor(path = "main-es5.js", requestType = RequestType.GET)
	public PureResponseMessage getMain_ES5Js() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "main-es5.js"))), "application/javascript ");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "application/javascript ");
		}
	}

	@PureRequestAcceptor(path = "main-es2015.js", requestType = RequestType.GET)
	public PureResponseMessage getMain_ES2015Js() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "main-es2015.js"))), "application/javascript ");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "application/javascript ");
		}
	}

	@PureRequestAcceptor(path = "polyfills-es5.js", requestType = RequestType.GET)
	public PureResponseMessage getPolyfills_ES5Js() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "polyfills-es5.js"))), "application/javascript ");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "application/javascript ");
		}
	}

	@PureRequestAcceptor(path = "polyfills-es2015.js", requestType = RequestType.GET)
	public PureResponseMessage getPolyfills_ES2015Js() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "polyfills-es2015.js"))), "application/javascript ");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "application/javascript ");
		}
	}

	@PureRequestAcceptor(path = "runtime-es5.js", requestType = RequestType.GET)
	public PureResponseMessage getRuntime_ES5Js() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "runtime-es5.js"))), "application/javascript ");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "application/javascript ");
		}
	}

	@PureRequestAcceptor(path = "runtime-es2015.js", requestType = RequestType.GET)
	public PureResponseMessage getRuntime_ES2015Js() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "runtime-es2015.js"))), "application/javascript ");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "application/javascript ");
		}
	}

	@PureRequestAcceptor(path = "styles-es5.js", requestType = RequestType.GET)
	public PureResponseMessage getStyles_ES5Js() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "styles-es5.js"))), "application/javascript ");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "application/javascript ");
		}
	}

	@PureRequestAcceptor(path = "styles-es2015.js", requestType = RequestType.GET)
	public PureResponseMessage getStyles_ES2015Js() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "styles-es2015.js"))), "application/javascript ");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "application/javascript ");
		}
	}

	@PureRequestAcceptor(path = "vendor-es5.js", requestType = RequestType.GET)
	public PureResponseMessage getVendor_ES5Js() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "vendor-es5.js"))), "application/javascript ");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "application/javascript ");
		}
	}

	@PureRequestAcceptor(path = "vendor-es2015.js", requestType = RequestType.GET)
	public PureResponseMessage getVendor_ES2015Js() {
		try {
			return new PureResponseMessage(ResponseType.OK, new TransferStream(Files.readAllBytes(Paths.get(BASE_PATH + "vendor-es2015.js"))), "application/javascript ");
		} catch (IOException e) {
			return new PureResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new TransferStream(new byte[0]), "application/javascript ");
		}
	}

}
